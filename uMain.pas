﻿unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.ImageList,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.ImgList, FMX.Effects, FMX.Filter.Effects, FMX.Layouts, FMX.Menus, FMX.Ani,
  FMX.StdCtrls,
  PK.TrayIcon;

type
  TfrmMain = class(TForm)
    imgNeko: TImage;
    imglstIcon: TImageList;
    effectInvert: TInvertEffect;
    effectGlow: TGlowEffect;
    layoutNekoBase: TLayout;
    menuPopup: TPopupMenu;
    menuExit: TMenuItem;
    imglstAniNeko: TImageList;
    timerAni: TTimer;
    aniColor: TColorAnimation;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure imgNekoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure menuExitClick(Sender: TObject);
    procedure timerAniTimer(Sender: TObject);
    procedure imgNekoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private const
    IMAGE_SIZE: TSizeF = (CX: 160; CY: 214);
    ICON_SIZE: TSizeF = (CX: 24; CY: 24);
    FREEZE_TIME = 1000;
    MOVE_DELTA = 32;
    MENU_VERSION = 'バージョン情報';
    MENU_EXIT = '終了';
    TRAY_ICON = 'Normal';
    TRAY_HINT = 'ねこです';
  private var
    FTrayIcon: TTrayIcon;
    FIndex: Integer;
  private
    procedure Terminate;
    procedure MenuExitHandler(Sender: TObject);
    procedure MenuVersionHandler(Sender: TObject);
  public
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

uses
  PK.Utils.Application, uVersion;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  Randomize;

  Application.HideTaskBar;

  FTrayIcon := TTrayIcon.Create;

  FTrayIcon.AddMenu(MENU_VERSION, MenuVersionHandler);
  FTrayIcon.AddMenu('-', nil);
  FTrayIcon.AddMenu(MENU_EXIT, MenuExitHandler);

  var Bmp := imglstIcon.Bitmap(ICON_SIZE, 0);
  FTrayIcon.RegisterIcon(TRAY_ICON, Bmp);
  FTrayIcon.ChangeIcon(TRAY_ICON, TRAY_HINT);

  FTrayIcon.Apply;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FTrayIcon.DisposeOf;
end;

procedure TfrmMain.imgNekoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  if (Button = TMouseButton.mbLeft) and not (ssDouble in Shift) then
  begin
    timerAni.Enabled := False;
    aniColor.Start;
    effectGlow.Enabled := True;
    StartWindowDrag;
  end;
end;

procedure TfrmMain.imgNekoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  if not timerAni.Enabled then
  begin
    aniColor.Stop;
    effectGlow.Enabled := False;
    timerAni.Enabled := True;
  end;
end;

procedure TfrmMain.MenuExitHandler(Sender: TObject);
begin
  Terminate;
end;

procedure TfrmMain.MenuVersionHandler(Sender: TObject);
begin
  TfrmVersion.Show;
end;

procedure TfrmMain.menuExitClick(Sender: TObject);
begin
  Terminate;
end;

procedure TfrmMain.Terminate;
begin
  timerAni.Enabled := False;

  effectGlow.GlowColor := TAlphaColors.Darkmagenta;
  effectGlow.Enabled := True;
  effectInvert.Enabled := True;

  TThread.CreateAnonymousThread(
    procedure
    begin
      Sleep(FREEZE_TIME);
      TThread.Synchronize(
        TThread.Current,
        procedure
        begin
          Close;
        end
      );
    end
  ).Start;
end;

procedure TfrmMain.timerAniTimer(Sender: TObject);
begin
  var Bmp := imglstAniNeko.Bitmap(IMAGE_SIZE, FIndex);
  imgNeko.Bitmap.Assign(Bmp);

  Inc(FIndex);
  if FIndex >= imglstAniNeko.Count then
    FIndex := 0;

  Left := Left - MOVE_DELTA;
  if Left + Width < Screen.WorkAreaLeft then
  begin
    Left := Screen.WorkAreaRect.Right;
    Top := Random(Screen.WorkAreaRect.Height - Height);
  end;
end;

end.
