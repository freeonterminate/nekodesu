﻿# ねこです　よろしくおねがいします

かわいいねこのアクセサリです。  
デスクトップ上を歩き回るよ。

![neko](https://bitbucket.org/freeonterminate/nekodesu/raw/2f6c7620e44a4bb54bb931fa6ff87ff9e55ff249/Images/neko1.png)  

# Original Image
Title: SCP-040-JP  
Source: http://ja.scp-wiki.net/scp-040-jp  
License: CC BY-SA 3.0  

# Derivative Image
Derivative: Animation  
Source: https://bitbucket.org/freeonterminate/nekodesu/src/master/Images/  
New Author: pik  
License: CC BY-SA 3.0  

# Source Code License
Copyright (C) 2018, pik.  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
