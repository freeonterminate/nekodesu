﻿unit uVersion;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects,
  PK.Utils.WebLabel, FMX.Layouts;

type
  TfrmVersion = class(TForm)
    lblNeko1: TLabel;
    lblNeko2: TLabel;
    lblVersion: TLabel;
    lineSep: TLine;
    txtOriginal: TText;
    lblCC: TLabel;
    btnClose: TButton;
    layoutButtonBase: TLayout;
    styleBlend: TStyleBook;
    procedure FormCreate(Sender: TObject);
  private const
    URL_SCP040JP = 'http://ja.scp-wiki.net/scp-040-jp';
    VERSION_FORMAT = 'Version %s';
  private
    FWebLabel: TWebLabel;
  public
    class procedure Show;
  end;

implementation

{$R *.fmx}

uses
  PK.Utils.Application;

procedure TfrmVersion.FormCreate(Sender: TObject);
begin
  FWebLabel :=
    TWebLabel.Create(
      txtOriginal,
      TAlphaColors.White,
      TAlphaColors.Aqua,
      TAlphaColors.Aqua,
      URL_SCP040JP);

  lblVersion.Text := Format(VERSION_FORMAT, [Application.Version]);
end;

class procedure TfrmVersion.Show;
begin
  with TfrmVersion.Create(nil) do
    try
      ShowModal;
    finally
      Close;
    end;
end;

end.
