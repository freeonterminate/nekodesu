(*
 * TWebLabel : TText as Linkked Text
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/04/11 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Utils.WebLabel;

interface

uses
  System.UITypes
  , System.Classes
  , FMX.Objects
  ;

type
  TWebLabel = class
  private var
    FURL: String;
    FText: TText;
    FFontStyles: TFontStyles;
    FNormal: TAlphaColor;
    FOver: TAlphaColor;
    FPressed: TAlphaColor;
    FDowned: Boolean;
  private
    function PtInText(const iX, iY: Single): Boolean;
    procedure Capture(const iCapture: Boolean);
    procedure SetTextFontColor(const iColor: TAlphaColor);
    procedure TextMouseDown(
      Sender: TObject;
      Button: TMouseButton;
      Shift: TShiftState;
      X, Y: Single);
    procedure TextMouseUp(
      Sender: TObject;
      Button: TMouseButton;
      Shift: TShiftState;
      X, Y: Single);
    procedure TextMouseMove(
      Sender: TObject;
      Shift: TShiftState;
      X, Y: Single);
  public
    constructor Create(
      const iText: TText;
      const iNormal, iOver, iPressed: TAlphaColor;
      const iURL: String); reintroduce;
  end;

implementation

uses
  System.Types
  , PK.Utils.Browser;

{ TWebLable }

procedure TWebLabel.Capture(const iCapture: Boolean);
begin
  if FText.Root = nil then
    Exit;

  if iCapture then
    FText.Root.Captured := FText
  else
    FText.Root.Captured := nil;
end;

constructor TWebLabel.Create(
  const iText: TText;
  const iNormal, iOver, iPressed: TAlphaColor;
  const iURL: String);
begin
  inherited Create;

  FText := iText;
  FNormal := iNormal;
  FOver := iOver;
  FPressed := iPressed;
  FURL := iURL;

  FFontStyles := FText.Font.Style;

  FText.Cursor := crHandPoint;
  FText.HitTest := True;
  FText.OnMouseDown := TextMouseDown;
  FText.OnMouseMove := TextMouseMove;
  FText.OnMouseUp := TextMouseUp;
end;

function TWebLabel.PtInText(const iX, iY: Single): Boolean;
begin
  Result := FText.LocalRect.Contains(TPointF.Create(iX, iY));
end;

procedure TWebLabel.SetTextFontColor(const iColor: TAlphaColor);
const
  STYLES: array [Boolean] of TFontStyles = ([TFontStyle.fsUnderline], []);
begin
  FText.TextSettings.FontColor := iColor;
  FText.TextSettings.Font.Style := FFontStyles + STYLES[iColor = FNormal];
end;

procedure TWebLabel.TextMouseDown(
  Sender: TObject;
  Button: TMouseButton;
  Shift: TShiftState;
  X, Y: Single);
begin
  FDowned := True;
  Capture(True);
  SetTextFontColor(FPressed);
end;

procedure TWebLabel.TextMouseMove(
  Sender: TObject;
  Shift: TShiftState;
  X, Y: Single);
var
  Color: TAlphaColor;
begin
  Color := FNormal;

  if PtInText(X, Y) then
  begin
    if FDowned then
      Color := FPressed
    else
    begin
      Color := FOver;
      Capture(True);
    end;
  end
  else
  begin
    if not FDowned then
      Capture(False);
  end;

  SetTextFontColor(Color);
end;

procedure TWebLabel.TextMouseUp(
  Sender: TObject;
  Button: TMouseButton;
  Shift: TShiftState;
  X, Y: Single);
begin
  FDowned := False;
  Capture(False);
  SetTextFontColor(FNormal);

  if PtInText(X, Y) then
    OpenBrowser(FURL);
end;

end.
